package com.vazhasapp.shemajamebeli3

import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.vazhasapp.shemajamebeli3.databinding.UserCardViewBinding

class UserAdapter(
    private val userList: MutableList<UserModel>,
    private val clickListener: UserOnClick
) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {

        return UserViewHolder(
            UserCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = userList.size

    inner class UserViewHolder(private val binding: UserCardViewBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var currentUser: UserModel
        fun bind() {
            currentUser = userList[adapterPosition]
            binding.tvUserName.text = currentUser.firstName
            binding.tvLastName.text = currentUser.lastName
            binding.tvEmail.text = currentUser.email
        }

        init {
            binding.btnEditUser.setOnClickListener(this)
            binding.btnRemoveUser.setOnClickListener(this)
            d("Adapter", "Still Called in the BindViewHolder")
        }

        override fun onClick(v: View?) {
            when(v!!.id) {
                R.id.btnRemoveUser -> clickListener.userRemoveClick(adapterPosition)
                R.id.btnEditUser -> clickListener.userUpdateClick(adapterPosition)
            }
        }
    }
}