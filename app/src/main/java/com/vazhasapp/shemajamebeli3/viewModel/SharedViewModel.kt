package com.vazhasapp.shemajamebeli3.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vazhasapp.shemajamebeli3.UserModel

class SharedViewModel : ViewModel() {

    private val userData = MutableLiveData<UserModel>()
    private val updatedUserData = MutableLiveData<UserModel>()

    // For add
    fun setUser(user: UserModel) {
        userData.value = user
    }

    fun getUser(): LiveData<UserModel> {
        return userData
    }

    // For update
    fun setUpdatedUser(user: UserModel) {
        updatedUserData.value = user
    }

    fun addUpdatedUser(): LiveData<UserModel> {
        return updatedUserData
    }
}