package com.vazhasapp.shemajamebeli3

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserModel(
    var firstName: String,
    var lastName: String,
    var email: String,
): Parcelable
