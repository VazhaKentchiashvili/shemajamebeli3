package com.vazhasapp.shemajamebeli3.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.vazhasapp.shemajamebeli3.*
import com.vazhasapp.shemajamebeli3.databinding.FragmentMainBinding
import com.vazhasapp.shemajamebeli3.viewModel.SharedViewModel

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private lateinit var myAdapter: UserAdapter
    private val userList = mutableListOf<UserModel>()
    private lateinit var sharedViewModel: SharedViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

    }

    private fun init() {
        // Set Up Recycler Items Click
        myAdapter = UserAdapter(userList, object : UserOnClick {
            override fun userRemoveClick(position: Int) {
                userList.removeAt(position)
                myAdapter.notifyItemRemoved(position)
            }

            override fun userUpdateClick(position: Int) {
                val direction = MainFragmentDirections.actionMainFragmentToUpdateFragment()
                findNavController().navigate(direction)

                sharedViewModel =
                    ViewModelProvider(requireParentFragment()).get(SharedViewModel::class.java)

                sharedViewModel.setUpdatedUser(userList[position])
                sharedViewModel.addUpdatedUser().observe(viewLifecycleOwner, {
                    userList[position] = it
                    myAdapter.notifyItemChanged(position)
                })
            }
        })

        // Add user in the list
        getAddedUser()

        binding.imbtnAddUser.setOnClickListener {
            val direction = MainFragmentDirections.actionMainFragmentToAddFragment()
            findNavController().navigate(direction)
        }

        // Set Up RecyclerView
        binding.myRecyclerView.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
    }

    // Get Added user form ViewModel
    private fun getAddedUser() {
        sharedViewModel =
            ViewModelProvider(requireParentFragment()).get(SharedViewModel::class.java)

        sharedViewModel.getUser().observe(viewLifecycleOwner, {
            userList.add(it)
            myAdapter.notifyItemInserted(userList.size)
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}