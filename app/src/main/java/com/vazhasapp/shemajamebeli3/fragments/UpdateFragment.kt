package com.vazhasapp.shemajamebeli3.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.vazhasapp.shemajamebeli3.viewModel.SharedViewModel
import com.vazhasapp.shemajamebeli3.UserModel
import com.vazhasapp.shemajamebeli3.databinding.FragmentAddBinding

class UpdateFragment : Fragment() {

    private var _binding: FragmentAddBinding? = null
    private val binding get() = _binding!!
    private lateinit var sharedViewModel: SharedViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

    }

    private fun init() {
        getUserWhichPrepareForUpdate()
        binding.btnSave.setOnClickListener {
            setUpdatedUser()
            findNavController().popBackStack()
        }
    }

    // This adding current user info in edit texts.
    private fun getUserWhichPrepareForUpdate() {
        sharedViewModel =
            ViewModelProvider(requireParentFragment()).get(SharedViewModel::class.java)

        sharedViewModel.addUpdatedUser().observe(viewLifecycleOwner, {
            binding.etFirstName.setText(it.firstName)
            binding.etLastName.setText(it.lastName)
            binding.etEmail.setText(it.email)
        })
    }

    private fun setUpdatedUser() {
        sharedViewModel =
            ViewModelProvider(requireParentFragment()).get(SharedViewModel::class.java)
        sharedViewModel.setUpdatedUser(getEnteredUser())
    }

    private fun getEnteredUser(): UserModel {
        val updatedFirstName = binding.etFirstName.text.toString()
        val updatedLastName = binding.etLastName.text.toString()
        val updatedEmail = binding.etEmail.text.toString()

        return UserModel(updatedFirstName, updatedLastName, updatedEmail)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}