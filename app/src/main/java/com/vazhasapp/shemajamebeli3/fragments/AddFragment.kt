package com.vazhasapp.shemajamebeli3.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.vazhasapp.shemajamebeli3.viewModel.SharedViewModel
import com.vazhasapp.shemajamebeli3.UserModel
import com.vazhasapp.shemajamebeli3.databinding.FragmentAddBinding

class AddFragment : Fragment() {

    private var _binding: FragmentAddBinding? = null
    private val binding get() = _binding!!
    private lateinit var sharedViewModel: SharedViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        binding.btnSave.setOnClickListener {
            findNavController().popBackStack()
            setEnteredUser()
        }
    }

    // Put new user in LiveData to add in RecyclerView
    private fun setEnteredUser() {
        sharedViewModel =
            ViewModelProvider(requireParentFragment()).get(SharedViewModel::class.java)
        sharedViewModel.setUser(getEnteredUser())
    }

    private fun getEnteredUser(): UserModel {
        val enteredFirstName = binding.etFirstName.text.toString()
        val enteredLastName = binding.etLastName.text.toString()
        val enteredEmail = binding.etEmail.text.toString()

        return UserModel(enteredFirstName, enteredLastName, enteredEmail)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}