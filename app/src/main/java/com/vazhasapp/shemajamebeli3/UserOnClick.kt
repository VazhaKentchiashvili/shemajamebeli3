package com.vazhasapp.shemajamebeli3

interface UserOnClick {
    fun userRemoveClick(position: Int)
    fun userUpdateClick(position: Int)
}